<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    //

    protected $hidden = ['questions'];

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
