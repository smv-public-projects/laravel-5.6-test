<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-AUTH-TOKEN');

        if ( $token != 'token-value')
        {
            return \response()->json('Auth token not found', 401);
            abort(Response::HTTP_UNAUTHORIZED , 'Auth token not found');
        }

        return $next($request);
    }
}
